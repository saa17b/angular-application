import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ProductService } from './product.service';
import { IProduct } from './products';

@Component({
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  pageTitle: string = 'Product Detail';
  product: IProduct | undefined;
  errorMessage: string = '';

  constructor(private route: ActivatedRoute,
              private router: Router,
              private productService: ProductService) { }

  ngOnInit(): void {
    const param = Number(this.route.snapshot.paramMap.get('id'));
    this.pageTitle += `: ${param}`;
    if(param) {
      const id = param;
      this.getProduct(id);
    }
  }

  getProduct(id: number): void {
     this.productService.getProduct(id).subscribe({
        next: product => this.product = product,
        error: err => this.errorMessage = err
     });
  }

  onBack(): void {
    this.router.navigate(['/products']);
  }

}
